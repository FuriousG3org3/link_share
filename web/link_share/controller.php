<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

class LinkSharePackage extends Package {

	protected $pkgHandle = 'link_share';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '0.5';
	
	public function getPackageDescription() {
		return t("This block is used to setup a place where the end user can copy embed used for sharing a link to this site.");
	}
	
	public function getPackageName() {
		return t("Link Share");
	}
	
	public function install() {
		$pkg = parent::install();
		
		//install blocks
	  	BlockType::installBlockTypeFromPackage('link_share', $pkg);	
		
	 }
}
?>