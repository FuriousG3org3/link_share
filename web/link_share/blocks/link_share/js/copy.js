//javscript for the clipboard button 
function getCopy() {
	ZeroClipboard.setMoviePath('localhost:8888/concrete5/packages/link_share/blocks/link_share/ZeroClipboard.swf');
	var clip = new ZeroClipboard.Client();
	clip.addEventListener('mousedown', function() {
		clip.setText(document.getElementById('link_share_id').value);
	});
	clip.addEventListener('complete', function(client, text){
		alert('copied', text);
	});
	clip.glue('copy_to_clipboard');
};

function copy_to_clipboard() {
	$('#copy_to_clipboard').click(function() {
		getCopy();
	});
};

$(function() {
	copy_to_clipboard();
});