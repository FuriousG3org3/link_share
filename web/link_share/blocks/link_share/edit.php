<?php  defined('C5_EXECUTE') or die("Access Denied.");
$al = Loader::helper('concrete/asset_library');
$ps = Loader::helper('form/page_selector');
//$form = Loader::helper('form');

$isChecked = empty($url_suffix_user) ? 0 : $url_suffix_user;
?>

<style type="text/css" media="screen">
	.ccm-block-field-group h2 { margin-bottom: 5px; }
	.ccm-block-field-group td { vertical-align: middle; }
</style>

<div class="ccm-block-field-group">
	<h2><?php echo t('Link Text'); ?></h2>
	<?php  echo $form->text('link_text', $link_text, array('style' => 'width: 95%;')); ?><br />
	<?php echo t('This is text that will be placed under the link image'); ?> 
</div>

<div class="ccm-block-field-group">
	<h2><?php echo t('Link Image'); ?></h2>
	<?php  echo $al->image('link_image_fID', 'link_image_fID', 'Choose Image', $link_image); ?>

	<table border="0" cellspacing="3" cellpadding="0" style="width: 95%;">
		<tr>
			<td align="right" nowrap="nowrap"><label for="link_image_internalLinkCID"><?php echo t('Link to Page:'); ?></label>&nbsp;</td>
			<td align="left" style="width: 100%;"><?php  echo $ps->selectPage('link_image_internalLinkCID', $link_image_internalLinkCID); ?></td>
		</tr>
		<tr>
			<td align="right" nowrap="nowrap"><label for="link_image_altText"><?php echo t('Alt Text:'); ?></label>&nbsp;</td>
			<td align="left" style="width: 100%;"><?php  echo $form->text('link_image_altText', $link_image_altText, array('style' => 'width: 100%;')); ?><br />
				<?php echo t('If no Alt text is given, link text will be used for Alt text'); ?>
			</td>
		</tr>
	</table>
	
</div>

<div class="ccm-block-field-group">
	<h2><?php echo t('URL Suffix'); ?></h2>
	<table border="0" cellspacing="3" cellpadding="0" style="width: 95%;">
		<tr>
			<td align="right" nowrap="nowrap"><label for="url_suffix"><?php echo t('Url Suffix:'); ?></label>&nbsp;</td>
			<td align="left" style="width: 100%;"><?php  echo $form->text('url_suffix', $url_suffix, array('style' => 'width: 95%;')); ?></td>
		</tr>
		<tr>
			<td align="right" nowrap="nowrap"><label for="url_suffix_user"><?php echo t('Append Logged in User to Suffix?:'); ?></label>&nbsp;</td>
			<td align="left" style="width: 100%;"><?php  echo $form->checkbox('url_suffix_user',1,$isChecked); echo t('This will append the username in the form of ref=username'); ?>
			</td>
		</tr>
	</table>
	
	<br />
	<?php echo t('A question mark(?) will automatically be added to the URL before your URL Suffix '); ?>
	
</div>


