ccmValidateBlockForm = function() {
	
	if ($('#link_image_fID-fm-value').val() == '' || $('#link_image_fID-fm-value').val() == 0) {
		ccm_addError('Missing required image: Link Image');
	}
	if ($('input[name=link_image_internalLinkCID]').val() == '' || $('input[name=link_image_internalLinkCID]').val() == 0) {
		ccm_addError('Missing required link: Link to Page');
	}

	return false;
}
