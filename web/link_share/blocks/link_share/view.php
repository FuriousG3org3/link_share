<?php  defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');

//Get page link
$pagelink =  $nh->getLinkToCollection(Page::getByID($link_image_internalLinkCID), true);


$imagetag = '<img src="'.$link_image->src.'"'. 
				' width="'.$link_image->width.'"'. 
				' height="'. $link_image->height.'"'. 
				' alt="';
if(empty($link_image_altText)) 
{
	$imagetag.= htmlentities($link_text,ENT_QUOTES, APP_CHARSET);
}else{
	$imagetag .= htmlentities($link_image_altText,ENT_QUOTES, APP_CHARSET);
}
	
$imagetag .=' border="0" />';	//close image tag

?>
<div class="link_share_container">
	<div class="link_share_preview">
		<?php echo $imagetag; ?>
	</div>
	<?php echo  $this->controller->embedCode($link_text,$pagelink, $link_image, $link_alt_text, $url_suffix, $url_suffix_user); ?> 
	<p><a href='#' id='copy_to_clipboard' name='copy_to_clipboard'>Copy to clipboard</a></p> <!--copy button-->
</div>