<?php  defined('C5_EXECUTE') or die("Access Denied.");

class LinkShareBlockController extends BlockController {
	
	protected $btName = 'Link Share';
	protected $btDescription = 'This block is used to setup a place where the end user can copy embed used for sharing a link to this site.';
	protected $btTable = 'btLinkShare';
	
	protected $btInterfaceWidth = "700";
	protected $btInterfaceHeight = "450";
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	

	public function view() {
		$this->set('link_image', (empty($this->link_image_fID) ? null : $this->get_image_object($this->link_image_fID, 0, 0, false)));
	}


	public function edit() {
		$this->set('link_image', (empty($this->link_image_fID) ? null : File::getByID($this->link_image_fID)));
	}

	public function save($args) {
		$args['url_suffix_user'] = empty($args['url_suffix_user']) ? 0 : $args['url_suffix_user'];
		$args['link_image_fID'] = empty($args['link_image_fID']) ? 0 : $args['link_image_fID'];
		$args['link_image_internalLinkCID'] = empty($args['link_image_internalLinkCID']) ? 0 : $args['link_image_internalLinkCID'];
		parent::save($args);
	}

	//Helper function for generating embed code
	public function embedCode($link_text,$linkurl, $link_image, $link_alt_text = '', $url_suffix='', $url_suffix_user=0){
		if(!empty($link_image)){
			//Get Sitename
			$sitename = Config::get('SITE');
			//Start embed code
			$embed = "<textarea cols=\"30\" rows=\"6\" class=\"link_share_embedcode\" id=\"link_share_id\"><!--Begin $sitename Link Share-->\n";
			$embed .= '<div style="width:'.($link_image->width+2) .'px;" >';
			$openlink = '<a href="'. $linkurl; 
			 	//Add Sufffix if provided but add ? first
			 	switch(true){
					case !empty($url_suffix) && $url_suffix_user:	//url_suffix and url_suffix_user both used
						$u = new User();
						$openlink .= '?'.$url_suffix.'&user='.$u->getUserName();
						break;
					case empty($url_suffix) && $url_suffix_user:	//just url_suffix_user is used
						$u = new User();
						$openlink .= '?user='.$u->getUserName();
						break;
					case !empty($url_suffix):						//just url_suffix is used
						$openlink .= '?'.$url_suffix;
			 	}
			$openlink .= '">';	//close opening anchor tag
			
			//Now we build the image tag
			
			$image = '<img src="'.$link_image->src.'"'. 
				' width="'.$link_image->width.'"'. 
				' height="'. $link_image->height.'"'. 
				' alt="';
			if(empty($link_image_altText)) 
			{
				$image.= htmlentities($link_text,ENT_QUOTES, APP_CHARSET);
			}else{
				$image .= htmlentities($link_image_altText,ENT_QUOTES, APP_CHARSET);
			}
				
			$image .=' border="0" />';	//close image tag
			
			$embed .= $openlink.$image.'</a>';
			//Now build check for link_text
			if(!empty($link_text))
				$embed .= '<div style="text-align:center;">'.$openlink.htmlentities($link_text,ENT_QUOTES, APP_CHARSET).'</a></div>';
			//Now lets close the embed code
			$embed .= "\n<!--End $sitename Link Share--></textarea>";
			
			//and finally return the results
			return $embed;
		}else{
			return "No Image set";
		}
	}

	//Helper function for image fields
	private function get_image_object($fID, $width = 0, $height = 0, $crop = false) {
		if (empty($fID)) {
			$image = null;
		} else if (empty($width) && empty($height)) {
			//Show image at full size (do not generate a thumbnail)
			$file = File::getByID($fID);
			$image = new stdClass;
			$image->src = $file->getRelativePath();
			$image->width = $file->getAttribute('width');
			$image->height = $file->getAttribute('height');
		} else {
			//Generate a thumbnail
			$width = empty($width) ? 9999 : $width;
			$height = empty($height) ? 9999 : $height;
			$file = File::getByID($fID);
			$ih = Loader::helper('image');
			$image = $ih->getThumbnail($file, $width, $height, $crop);
		}
	
		return $image;
	}
	


}
